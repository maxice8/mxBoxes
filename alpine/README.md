## Releases

It will by default build a container against alpine:edge.

This can be changed by using `RELEASE=$VERSION`, where
`$VERSION` is the stable release number, like 3.13, or edge.

Remember to use give it another `PORT=` so you can run multiple
containers in parallel.

- The systemd service and will have `-$VERSION` as suffix
- The abuild binary for versions that are not edge will also have the `-$VERSION` suffix
- Packages will be installed in a subdirectory of the default REPODEST ($HOME/packages), that
  subdirectory named `$VERSION`
