#!/bin/sh
die() {
	printf -- "\033[0m[ \033[31mERR\033[0m ] %s\\n" "$@" >&2
	exit 1
}

warn() {
	printf -- "\033[0m[ \033[33mWARN\033[0m ] %s\\n" "$@"
}

run() {
	ssh -p %%PORT%% -tt \
		-o User="$USER" \
		-o IdentityFile=~/.ssh/alpine-dev-env \
		-o NoHostAuthenticationForLocalhost=yes \
		-o LogLevel=QUIET \
		-o IdentitiesOnly=yes \
		localhost "$@"
}

: ${APORTSDIR:=%%APORTSDIR%%}

APORTSDIRNAME="$(basename $APORTSDIR)"

## check running from within an `aports` tree
if [ "${PWD%*/$APORTSDIRNAME/*}" = "$PWD" ]; then
	die "Error: expecting to be run from within an aports tree!" \
		"It must be run under '$APORTSDIR'"
fi

# Generate new abuild key if not set
cmd="grep -sq '^PACKAGER_PRIVKEY=' $HOME/.abuild/abuild.conf || abuild-keygen -n -a"

# Copy keys over, we need this so packages will actually succeed when
# building (because of the signing stage), this is also required to
# install them
cmd="$cmd && sudo cp $HOME/.abuild/*.rsa.pub /etc/apk/keys"

case "$1" in
	checksum|unpack) cmd="$cmd; cd $APORTSDIR/${PWD#*/$APORTSDIRNAME/} && abuild $*" ;;
	*) cmd="$cmd && cd $APORTSDIR/${PWD#*/$APORTSDIRNAME/} && sudo apk upgrade -U -a && abuild $*" ;;
esac

if [ "%%INITCTL%%" != false ]; then
	if ! %%INITCTL%% --user status alpine-ssh-%%RELEASE%% | grep -q start/running; then
		warn 'alpine-ssh-%%RELEASE%% is not running, starting it'
		if ! %%INITCTL%% --user start alpine-ssh-%%RELEASE%%; then
			die "couldn't start alpine-ssh-%%RELEASE%%, aborting"
		fi
	fi
fi

if [ "%%SYSTEMCTL%%" != false ]; then
	if ! %%SYSTEMCTL%% --user is-active --quiet alpine-ssh-%%RELEASE%%.service; then
		warn "alpine-ssh-%%RELEASE%%.service is not running, starting it"
		if ! %%SYSTEMCTL%% --user start --quiet alpine-ssh-%%RELEASE%%.service; then
			die "couldn't start alpine-ssh-%%RELEASE%%, aborting"
		fi
	fi
fi

run "$cmd"
